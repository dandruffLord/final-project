Sorry I couldn't get my binary to work in time, I hope this will net me something, I tried hard to make a good game.

Build Instructions:
Clone the repo.
Initialize all the necessary submodules:
//
cd project2
git submodule update --init --recursive
//

Build the godot-cpp bindings:
//
cd godot-cpp
scons platform=(platform) generate_bindings=yes -j(number of cores)
cd ..
//

Finally, build the game:
//
scons platform=(platform)
//