#ifndef HUMAN_H
#define HUMAN_H

#include <Godot.hpp>
#include <player.h>

namespace godot {

class Human : public Player {
    GODOT_CLASS(Human, Player)

protected:
    Action track_input() override;
    void shoot_while_moving() override;

public:
    static void _register_methods();
    Human();
    ~Human();
    void _init();
};

}

#endif