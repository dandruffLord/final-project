#include "human.h"

using namespace godot;

void Human::_register_methods() {
}

Human::Human() {
}

Human::~Human() {
    // add your cleanup here
}

void Human::_init() {
    Player::_init();
}

Player::Action Human::track_input() {
    if (input->is_action_just_pressed(action_inputs[MOVE_LEFT])) {
        return MOVE_LEFT;
    }
    if (input->is_action_just_pressed(action_inputs[MOVE_RIGHT])) {
        return MOVE_RIGHT;
    }
    if (input->is_action_just_pressed(action_inputs[MOVE_UP])) {
        return MOVE_UP;
    }
    if (input->is_action_just_pressed(action_inputs[MOVE_DOWN])) {
        return MOVE_DOWN;
    }
    if (input->is_action_just_pressed(action_inputs[SHOOT])) {
        return SHOOT;
    }
    return DO_NOTHING;
}

void Human::shoot_while_moving() {
    if (ammunition_count > 0 && input->is_action_just_pressed(action_inputs[SHOOT])) {
        is_invulnerable = true;
        fire_bullet();
    }
}