#include "computer.h"

using namespace godot;

void Computer::_register_methods() {
}

Computer::Computer() {
}

Computer::~Computer() {
}

void Computer::_init() {
    Player::_init();
}

Player::Action Computer::track_input() {
    // return DO_NOTHING;
    return static_cast<Action>(rand() % action_length);
}

void Computer::shoot_while_moving() {
    if (ammunition_count > 0 && (rand() % 2) == 0) {
        is_invulnerable = true;
        fire_bullet();
    }
}