#ifndef BULLET_H
#define BULLET_H

#include <Godot.hpp>
#include <Area2D.hpp>
#include <AnimatedSprite.hpp>
#include "player.h"

namespace godot {

class Bullet : public Area2D {
    GODOT_CLASS(Bullet, Area2D)

private:
    class Timer {
    private:
        int goal;
        int current;
    public:
        Timer(int init_goal = 0);
        void tick();
        bool is_timer_done();
        void restart_timer();
        int get_current();
        int get_goal();
    };
    Timer move_frames;

    AnimatedSprite* sprite;

    float amplitude;

public:
    static void _register_methods();
    Bullet();
    ~Bullet();
    void _init();
    void _ready();
    void _physics_process(float delta);

    void on_area_entered(Variant area);
};

}

#endif