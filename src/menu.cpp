#include "menu.h"

using namespace godot;

void Menu::_register_methods() {
    register_method("_ready", &Menu::_ready);
    register_method("on_start_button_pressed", &Menu::on_start_button_pressed);
    register_method("on_tips_button_pressed", &Menu::on_tips_button_pressed);
    register_method("on_quit_button_pressed", &Menu::on_quit_button_pressed);
    register_method("start_game", &Menu::start_game);
}

Menu::Menu() {
}

Menu::~Menu() {
}

void Menu::_init() {
    input = Input::get_singleton();
}

void Menu::_ready() {
    get_node("Start Button")->connect("pressed", this, "on_start_button_pressed", Array(), CONNECT_ONESHOT);
    get_node("Tips Button")->connect("pressed", this, "on_tips_button_pressed", Array(), CONNECT_ONESHOT);
    get_node("Quit Button")->connect("pressed", this, "on_quit_button_pressed");
}

void Menu::_process() {
    if (input->is_action_just_pressed("quit")) {
        get_tree()->quit();
    }
}

void Menu::on_start_button_pressed() {
    AnimatedSprite* loading_sprite = cast_to<AnimatedSprite>(get_node("Loading Screen"));
    loading_sprite->connect("frame_changed", this, "start_game");
    loading_sprite->show();
    loading_sprite->play();
}

void Menu::start_game() {
    int speed_settings_index = (int) cast_to<Range>(get_node("Sides/Setting/Speed"))->get_value();
    float speed_settings[4] = {.5, 1, 1.5, 2};
    ResourceLoader* loader = ResourceLoader::get_singleton();
    Ref<PackedScene> game_scene = loader->load("res://scenes/game.tscn");
    Ref<PackedScene> human_scene = loader->load("res://scenes/human.tscn");
    Node* game = game_scene->instance();
    Player* human1 = cast_to<Player>(human_scene->instance());
    Player* human2 = cast_to<Player>(human_scene->instance());
    human1->is_leader = true;
    human1->speed = speed_settings[speed_settings_index];
    human2->is_leader = false;
    human2->speed = speed_settings[speed_settings_index];
    get_tree()->get_root()->add_child(game);
    game->add_child(human1);
    game->add_child(human2);
    queue_free();
}

void Menu::on_tips_button_pressed() {
    cast_to<CanvasItem>(get_node("Tips"))->show();
}

void Menu::on_quit_button_pressed() {
    get_tree()->quit();
}