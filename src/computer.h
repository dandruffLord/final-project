#ifndef COMPUTER_H
#define COMPUTER_H

#include <Godot.hpp>
#include <player.h>

namespace godot {

class Computer : public Player {
    GODOT_CLASS(Computer, Player)

protected:
    Action track_input() override;
    void shoot_while_moving() override;

public:
    static void _register_methods();
    Computer();
    ~Computer();
    void _init();
};

}

#endif