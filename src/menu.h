#ifndef MENU_H
#define MENU_H

#include <Godot.hpp>
#include <Node2D.hpp>
#include <Input.hpp>
#include <SceneTree.hpp>
#include <ResourceLoader.hpp>
#include <PackedScene.hpp>
#include <Viewport.hpp>
#include <Range.hpp>
#include "human.h"
#include "computer.h"

namespace godot {

class Menu : public Node2D {
    GODOT_CLASS(Menu, Node2D)

protected:
    Input* input;

public:
    static void _register_methods();
    Menu();
    ~Menu();
    void _init();
    void _process();
    void _ready();

    void on_start_button_pressed();
    void start_game();
    void on_tips_button_pressed();
    void on_quit_button_pressed();
};

}

#endif