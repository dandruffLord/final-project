#ifndef PLAYER_H
#define PLAYER_H

#include <Godot.hpp>
#include <Area2D.hpp>
#include <Input.hpp>
#include <AnimatedSprite.hpp>
#include <SpriteFrames.hpp>
#include <PackedScene.hpp>
#include <ResourceLoader.hpp>
#include <SceneTree.hpp>
#include <Viewport.hpp>
#include <list>
#include "bullet.h"

namespace godot {

class Player : public Area2D {
    GODOT_CLASS(Player, Area2D)

protected:
    enum State {
        IDLING,
        REFILLING,
        MOVING,
        TELEPORTING,
        SHOOTING,
        BREAKING
    };
    State state;
    void set_state(State new_state);
    void set_state_to_idling_or_refilling();
    void set_state_to_moving();
    void set_state_to_teleporting();
    void set_state_to_shooting();
    void set_state_to_breaking();
    void idle();
    void refill();
    void move();
    void teleport();
    void shoot();

    enum Action {
        MOVE_LEFT,
        MOVE_RIGHT,
        MOVE_UP,
        MOVE_DOWN,
        SHOOT,
        DO_NOTHING,
    };
    int action_length;
    Array action_inputs;
    Action current_action;
    void act_on_input();
    virtual Action track_input();
    void perform_action(Action action);

    class Timer {
    private:
        int goal;
        int current;
    public:
        Timer(int init_goal = 0);
        void tick();
        bool is_timer_done();
        void restart_timer();
        int get_current();
        int get_goal();
    };
    Timer refill_frames;
    Timer move_frames;
    Timer teleport_frames;
    Timer shoot_frames;

    Input* input;
    AnimatedSprite* sprite;
    Ref<SpriteFrames> sprite_frames;
    Node2D* weapon;
    Ref<PackedScene> bullet_scene;
    AnimatedSprite* ammunition_counter;

    Vector2 board_dimensions;
    Vector2 ammunition_cache_position;
    float teleport_distance;

    Vector2 position;
    Vector2 weapon_orientation;
    Vector2 move_direction;
    Vector2 move_vector;
    int ammunition_count;
    bool is_invulnerable;
    bool is_pushing;

    void time_machine();
    void differentiate_Players();
    void set_weapon_rotation();
    void set_invulnerability(bool value);
    Vector2 get_tile_position(Vector2 board_position);
    void change_ammunition_count(int offset);
    void handle_pushing(Player* pusher, Player* faller);
    virtual void shoot_while_moving();
    void fire_bullet();

public:
    static void _register_methods();
    Player();
    ~Player();
    void _init();
    void _ready();
    void _physics_process(float delta);

    float speed;
    bool is_leader;
    
    int tweak_Timer_frames(int frames);
    void on_area_entered(Variant area);
    void end_game();
    void replay_game();
};

}

#endif