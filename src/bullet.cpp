#include "bullet.h"

using namespace godot;

void Bullet::_register_methods() {
    register_method("_physics_process", &Bullet::_physics_process);
    register_method("on_area_entered", &Bullet::on_area_entered);
    register_method("_ready", &Bullet::_ready);
}

Bullet::Bullet() {
}

Bullet::~Bullet() {
}

void Bullet::_init() {
    amplitude = 1000;
    connect("area_entered", this, "on_area_entered");
}

void Bullet::_ready() {
    Node* root = get_parent()->get_parent()->get_parent();
    Transform2D transform = get_global_transform();
    move_frames = Timer(cast_to<Player>(get_parent()->get_parent())->tweak_Timer_frames(30));
    move_frames.restart_timer();
    get_parent()->remove_child(this);
    root->add_child(this);
    set_global_transform(transform);
}

void Bullet::_physics_process(float delta) {
    if (!move_frames.is_timer_done()) {
        Vector2 move_direction = Math::polar2cartesian(Vector2(1, Math::deg2rad(get_global_rotation_degrees())));
        global_translate(move_direction * amplitude * (1.0/move_frames.get_goal()));
        move_frames.tick();
    }
    else {
        queue_free();
    }
}

void Bullet::on_area_entered(Variant area) {
    Bullet* bullet = cast_to<Bullet>(area);
    if (bullet) {
        bullet->queue_free();
        queue_free();
    }
}

Bullet::Timer::Timer(int init_goal) {
    goal = init_goal;
    current = init_goal;
}

void Bullet::Timer::tick() {
    current = fmin(current + 1, goal);
}

bool Bullet::Timer::is_timer_done() {
    return (current == goal);
}

void Bullet::Timer::restart_timer() {
    current = 0;
}

int Bullet::Timer::get_current() {
    return current;
}

int Bullet::Timer::get_goal() {
    return goal;
}