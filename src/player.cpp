#include "player.h"

using namespace godot;

void Player::_register_methods() {
    register_method("_ready", &Player::_ready);
    register_method("_physics_process", &Player::_physics_process);
    register_method("on_area_entered", &Player::on_area_entered);
    register_method("end_game", &Player::end_game);
    register_method("replay_game", &Player::replay_game);
}

Player::Player() {
}

Player::~Player() {
}

void Player::_init() {
    teleport_distance = 130;
    board_dimensions = Vector2(5,3);
    action_length = 6;
    ammunition_cache_position = Vector2(2,1);
    speed = 1;

    position = Vector2(0,1);
    ammunition_count = 0;
    weapon_orientation = Vector2::RIGHT;
    move_direction = Vector2::ZERO;
    is_pushing = false;
    is_invulnerable = false;

    input = Input::get_singleton();
    connect("area_entered", this, "on_area_entered");
}

void Player::_ready() {
    sprite = cast_to<AnimatedSprite>(get_node("AnimatedSprite"));
    sprite_frames = sprite->get_sprite_frames();
    weapon = cast_to<Node2D>(get_node("Weapon"));
    bullet_scene = ResourceLoader::get_singleton()->load("res://scenes/bullet.tscn", "PackedScene", true);

    time_machine();
    differentiate_Players();
    set_global_position(get_tile_position(position));
    set_weapon_rotation();
    set_state_to_idling_or_refilling();
}

void Player::time_machine() {
    refill_frames = Timer(tweak_Timer_frames(30));
    sprite_frames->set_animation_speed("refill", (60 * sprite_frames->get_frame_count("refill") / ((float) refill_frames.get_goal())));
    move_frames = Timer(tweak_Timer_frames(16));
    sprite_frames->set_animation_speed("move", (60 * sprite_frames->get_frame_count("move") / ((float) move_frames.get_goal())));
    teleport_frames = Timer(tweak_Timer_frames(12));
    sprite_frames->set_animation_speed("teleport", (60 * sprite_frames->get_frame_count("teleport") / ((float) teleport_frames.get_goal())));
    shoot_frames = Timer(tweak_Timer_frames(14));
    sprite_frames->set_animation_speed("cock", (60 * sprite_frames->get_frame_count("cock") / ((float) shoot_frames.get_goal()/2)));
    sprite_frames->set_animation_speed("fire", (60 * sprite_frames->get_frame_count("fire") / ((float) shoot_frames.get_goal()/2)));
}

int Player::tweak_Timer_frames(int frames) {
    frames = (int) frames / speed;
    frames -= frames % 2;
    return frames;
}

void Player::differentiate_Players() {
    if (is_leader) {
        ammunition_counter = cast_to<AnimatedSprite>(get_node("../Left Ammo/Counter/Count"));
        cast_to<CanvasItem>(get_node("Weapon/Sprite"))->set_modulate(Color::html("#ff007f"));
        action_inputs = Array::make("leader_move_left", "leader_move_right", "leader_move_up", "leader_move_down", "leader_shoot");
        position = Vector2(1,1);
        weapon_orientation = Vector2::RIGHT;
    }
    else {
        ammunition_counter = cast_to<AnimatedSprite>(get_node("../Right Ammo/Counter/Count"));
        cast_to<CanvasItem>(get_node("Weapon/Sprite"))->set_modulate(Color::html("#66ff00"));
        action_inputs = Array::make("follower_move_left", "follower_move_right", "follower_move_up", "follower_move_down", "follower_shoot");
        position = Vector2(3,1);
        weapon_orientation = Vector2::LEFT;
    }
}

void Player::_physics_process (float delta) {
    if (input->is_action_just_pressed("quit")) {
        get_tree()->quit();
    }
    switch (state) {
        case IDLING: idle(); break;
        case REFILLING: refill(); break;
        case MOVING: move(); break;
        case TELEPORTING: teleport(); break;
        case SHOOTING: shoot(); break;
        case BREAKING:
        default: break;
    }
}

void Player::set_state(State new_state) {
    state = new_state;
}

void Player::set_state_to_idling_or_refilling() {
    move_direction = Vector2::ZERO;
    if (position == ammunition_cache_position) {
        refill_frames.restart_timer();
        sprite_frames->set_animation_loop("refill", true);
        sprite->play("refill");
        set_state(REFILLING);
    }
    else {
        sprite_frames->set_animation_loop("idle", true);
        sprite->play("idle");
        set_state(IDLING);
    }
}

void Player::idle() {
    act_on_input();
}

void Player::refill() {
    refill_frames.tick();
    if (refill_frames.is_timer_done()) {
        change_ammunition_count(1);
        refill_frames.restart_timer();
    }
    act_on_input();
}

void Player::set_state_to_moving() {
    move_vector = get_tile_position(position) - get_tile_position(position - move_direction);
    move_frames.restart_timer();
    sprite_frames->set_animation_loop("move", false);
    sprite->play("move");
    set_state(MOVING);
}

void Player::move() {
    global_translate(move_vector * (1.0/move_frames.get_goal()));
    move_frames.tick();
    if (move_frames.is_timer_done()) {
        is_pushing = false;
        is_invulnerable = false;
        set_state_to_idling_or_refilling();
    }
    if (!is_pushing) {
        shoot_while_moving();
    }
}

void Player::shoot_while_moving() {
}

void Player::set_state_to_teleporting() {
    teleport_frames.restart_timer();
    sprite_frames->set_animation_loop("teleport", false);
    sprite->play("teleport");
    set_state(TELEPORTING);
}

void Player::teleport() {
    global_translate(move_direction * teleport_distance * (1.0/teleport_frames.get_goal()));
    teleport_frames.tick();
    if (teleport_frames.is_timer_done()) {
        set_state_to_idling_or_refilling();
    }
    else if (teleport_frames.get_current() == teleport_frames.get_goal()/2) {
        set_global_position(get_tile_position(position) - (move_direction * teleport_distance/2));
        set_weapon_rotation();
        sprite->set_frame(0);
        sprite->play("teleport");
    }
}

void Player::set_state_to_shooting() {
    is_invulnerable = true;
    shoot_frames.restart_timer();
    sprite->play("cock");
    set_state(SHOOTING);
}

void Player::shoot() {
    shoot_frames.tick();
    if (shoot_frames.is_timer_done()) {
        is_invulnerable = false;
        set_state_to_idling_or_refilling();
    }
    else if (shoot_frames.get_current() == shoot_frames.get_goal()/2) {
        fire_bullet();
    }
}

void Player::fire_bullet() {
    sprite->play("fire");
    change_ammunition_count(-1);
    weapon->add_child(bullet_scene->instance());
}

void Player::set_state_to_breaking() {
    sprite->connect("animation_finished", this, "end_game", Array(), CONNECT_ONESHOT);
    sprite_frames->set_animation_loop("break", false);
    sprite->play("break");
    set_state(BREAKING);
}

void Player::end_game() {
    AnimatedSprite* replay_screen = cast_to<AnimatedSprite>(get_node("../Replay Screen"));
    if (replay_screen->is_visible()) {
        return;
    }
    replay_screen->set_frame(is_leader ? 1 : 0);
    replay_screen->show();
    replay_screen->get_node("Replay Button")->connect("pressed", this, "replay_game", Array(), CONNECT_ONESHOT);
}

void Player::replay_game() {
    Ref<PackedScene> menu_scene = ResourceLoader::get_singleton()->load("res://scenes/menu.tscn");
    get_tree()->get_root()->add_child(menu_scene->instance());
    get_parent()->queue_free();
}

void Player::act_on_input() {
    perform_action(track_input());
}

Player::Action Player::track_input() {
    return DO_NOTHING;
}

void Player::perform_action(Action action) {
    bool has_teleported = false;
    bool has_moved = false;
    current_action = action;

    switch(action) {
        case MOVE_LEFT:
            if (position.x == 0) {
                has_teleported = true;
                position.x = board_dimensions.x - 1;
                weapon_orientation = Vector2::LEFT;
            }
            else {
                has_moved = true;
                position.x--;
            }
            move_direction = Vector2::LEFT;
            break;
        case MOVE_RIGHT:
            if (position.x == board_dimensions.x - 1) {
                has_teleported = true;
                position.x = 0;
                weapon_orientation = Vector2::RIGHT;
            }
            else {
                has_moved = true;
                position.x++;
            }
            move_direction = Vector2::RIGHT;
            break;
        case MOVE_UP:
            if (position.y == 0) {
                has_teleported = true;
                position.y = board_dimensions.y - 1;
                weapon_orientation = Vector2::UP;
            }
            else {
                has_moved = true;
                position.y--;
            } 
            move_direction = Vector2::UP;
            break;
        case MOVE_DOWN:
            if (position.y == board_dimensions.y - 1) {
                has_teleported = true;
                position.y = 0;
                weapon_orientation = Vector2::DOWN;
            }
            else {
                has_moved = true;
                position.y++;
            }
            move_direction = Vector2::DOWN;
            break;
        case SHOOT:
            if (ammunition_count > 0) {
                set_state_to_shooting();
            }
            break;
        case DO_NOTHING:
        default:
            break;
    }

    if (has_teleported) {
        set_state_to_teleporting();
    }
    else if (has_moved) {
        set_state_to_moving();
    }
}

Vector2 Player::get_tile_position(Vector2 board_position) {
    String tile_name = String::num_int64((board_dimensions.x * board_position.y + board_position.x) + 1);
    NodePath path_to_tile = NodePath("../Board/Tiles/" + tile_name);
    return cast_to<Node2D>(get_node(path_to_tile))->get_global_position();
}

void Player::change_ammunition_count(int offset) {
    ammunition_count = fmin(3, fmax(0, ammunition_count + offset));
    ammunition_counter->set_frame(ammunition_count);
}

void Player::set_weapon_rotation() {
    weapon->set_global_rotation_degrees(Math::rad2deg(Math::cartesian2polar(weapon_orientation).y));
}

void Player::on_area_entered(Variant area) {
    Bullet* bullet = cast_to<Bullet>(area);
    if (!is_invulnerable && bullet) {
        bullet->queue_free();
        set_state_to_breaking();
    }
    else if (is_leader){
        Player* player = cast_to<Player>(area);
        if (player) {
            Vector2 position_difference = player->get_global_position() - get_global_position();
            if ((position_difference - move_direction).abs() < (-position_difference - player->move_direction).abs()) {
                handle_pushing(this, player);
            }
            else {
                handle_pushing(player, this);
            }
        }
    }
}

void Player::handle_pushing(Player* pusher, Player* faller) {
    pusher->is_pushing = true;
    faller->is_invulnerable = false;
    faller->position = pusher->position;
    faller->set_global_position(get_tile_position(pusher->position));
    if (faller->state != BREAKING) {
        faller->perform_action(pusher->current_action);
    }
}

Player::Timer::Timer(int init_goal) {
    goal = init_goal;
    current = init_goal;
}

void Player::Timer::tick() {
    current = fmin(current + 1, goal);
}

bool Player::Timer::is_timer_done() {
    return (current == goal);
}

void Player::Timer::restart_timer() {
    current = 0;
}

int Player::Timer::get_current() {
    return current;
}

int Player::Timer::get_goal() {
    return goal;
}